;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; REST API bindings for git.sr.ht
;;;
;;; Copyright (c) 2020, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare (module (sourcehut git))
         (import (except (scheme) log))
         (export artifact log refs))

(import (chicken format)
        (chicken keyword)
        (chicken type)
        (sourcehut)
        (srfi 1))

(define-inline (make-crud path #!optional (body '()))
  `((#:service "git" #:path ,path) . ,body))

(define-inline (make-form-crud path form)
  `((#:service "git" #:path ,path #:form (,form))))

;;
;; https://man.sr.ht/git.sr.ht/api.md#get-apireposnamerefs
;; https://man.sr.ht/git.sr.ht/api.md#get-apiusernamereposnamerefs
;;
(: refs (string #!optional string -> (list-of pair)))
(define (refs username-or-repo #!optional repo)
  (make-crud
   (if (not repo)
       (format "/api/repos/~A/refs" username-or-repo)
       (format "/api/~A/repos/~A/refs" username-or-repo repo))))

;;
;; https://man.sr.ht/git.sr.ht/api.md#get-apireposnamelog
;; https://man.sr.ht/git.sr.ht/api.md#get-apiusernamereposnamelog
;;
(: log (string #!optional string -> (list-of pair)))
(define (log username-or-repo #!optional repo)
  (make-crud
   (if (not repo)
       (format "/api/repos/~A/log" username-or-repo)
       (format "/api/~A/repos/~A/log" username-or-repo repo))))

;;
;; https://man.sr.ht/git.sr.ht/api.md#post-apireposnameartifactsref
;; https://man.sr.ht/git.sr.ht/api.md#post-apiusernamereposnameartifactsref
;;
(: artifact (string string #!optional string #!rest any -> (list-of pair)))
(define (artifact username-or-repo repo-or-ref #!optional ref #!rest args)
  (cond ((string? ref)
         (make-form-crud
          (format "/api/~A/repos/~A/artifacts/~A" username-or-repo repo-or-ref ref)
          (cons 'file args)))
        ((get-keyword #:file (cons ref args))
         (make-form-crud
          (format "/api/repos/~A/artifacts/~A" username-or-repo repo-or-ref)
          (cons* 'file ref args)))
        (else
         (signal-condition
          '(sourcehut)
          '(arity)
          '(exn location paste message "artifact #:file must be given")))))
