% sourcehut(1) Version 0.3.4 | chicken-sourcehut User Manual
% Evan Hanson <evhan@foldling.org>
% January 2021

# NAME

**sourcehut** - sourcehut API client

# SYNOPSIS

**sourcehut** *command* [*options* ...]

# DESCRIPTION

**sourcehut** is a command line program for interacting with the sourcehut API.

# OPTIONS

`--access-token <token>`
: The access token to use when authenticating to the Sourcehut API. This must be
provided for the program to work, either via this option or the
`SOURCEHUT_CLI_ACCESS_TOKEN` environment variable. New tokens can be generated
from your account's [OAuth settings page](https://meta.sr.ht/oauth). If
`<token>` contains a forward slash, it is treated as a file containing an access
token. Otherwise, `<token>` should be the access token itself.

`-n`, `--max-results <count>`
: Specify the number of results to include when retrieving a paginated resource
type.

`-o`, `--output <format>`
: Specify the output format for the command. `<format>` can be one of `j` (for
`json`) or `s` (the default), which generates S-expressions.

`-h`, `--help`
: Display program usage information and a list of supported commands.

`--version`
: Display the program version and exit.

# COMMANDS

## BUILDS

`get jobs`
: Fetch a list of jobs.

`get job <id>`
: Fetch a job by ID.

`get manifest <id>`
: Fetch the manifest for a job. The raw YAML manifest will be printed to
standard output.

`cancel job <id>`
: Cancel a job by ID.

`create job`
: Create a new job from a manifest. Creating a job triggers a new pipeline on
the [sr.ht builds service](https://builds.sr.ht). The new job will be started
immediately, and will have access to build secrets. This command accepts the
following arguments:

    `-f`, `--manifest <input>`
    : Specify a YAML build manifest file. If not given, or if `<input>` is "-",
    a manifest is read from standard input. Note the input YAML will be
    submitted as-is, without validation.

    `-n`, `--note <message>`
    : Provide a message to associate with this job. This will appear as a
    description on the build's page.

    `-t`, `--tags <tags>`
    : Provide a list of comma-separated tags to help identify this job. Each
    tag must use only lowercase alphanumeric characters, or any of "-_.".

## GIT

`get refs <repo>`
: Fetch a list of references in the given repository.

`create artifact <repo> <ref>`
: Attach an artifact to a reference in the given repository. This will upload
the given file and make it available for download. This is often used to
associate release artifacts with a given tag. This command accepts the
following arguments:

    `-f`, `--filename <input>`
    : Specify the file to upload. If not given, or if `<input>` is "-", the
    file is read from standard input.

    `-n`, `--name <filename>`
    : Provide a name for the file. If not given, the basename of the `<input>`
    argument is used. This option is required when `<input>` is read from
    standard input.

## META

`get profile`
: Fetch details for the authenticated user.

`get audit-log`
: Fetch a log of activity in the authenticated account.

`get ssh-keys`
: Fetch a list of SSH keys.

`get pgp-keys`
: Fetch a list of PGP keys.

`create ssh-key`
: Register a new SSH public key.

    `-f, --filename <input>`
    : Specify the SSH key to upload. If not given, or if `<input>` is "-", the
    key is read from standard input.

`create pgp-key`
: Register a new PGP public key.

    `-f, --filename <input>`
    : Specify the PGP key to upload. If not given, or if `<input>` is "-", the
    key is read from standard input.

`delete ssh-key <id>`
: Delete an SSH key. `<id>` should be the numeric ID of the key resource as
shown by `get ssh-keys`.

`delete pgp-key <id>`
: Delete a PGP key. `<id>` should be the numeric ID of the key resource as
shown by `get pgp-keys`.

## PASTE

`get pastes`
: Fetch a list of pastes.

`get paste <sha>`
: Fetch a paste by its SHA-1 checksum. Note the response will be a paste
resource, not the paste contents itself. To retrieve paste contents, use `get
blob`.

`get blob <id>`
: Fetch the contents of a file in a paste by its blob ID.

`create paste`
: Create a new paste. This command accepts the following arguments:

    `-f`, `--contents <input>`
    : Specify the paste content, which must be a UTF-8 encoded string. If not
    given, or if `<input>` is "-", paste content is read from standard input.

    `-n`, `--name <name>`
    : Specify an optional name for the paste.

    `-v`, `--visibility <level>`
    : Set the access level of the paste, one of "public", "private", or
    "unlisted". If not given, "unlisted" is used.

`paste`
: Create a new paste. This command works just like the `create paste` command,
but it prints a direct URL to the paste's contents rather than a representation
of the paste resource.

## TODO

`get trackers`
: Fetch a list of trackers.

`get tracker <name>`
: Fetch detailed information about a specific tracker. The argument should be a
tracker name.

`get tickets <tracker>`
: Fetch a list of tickets in the given tracker. The argument should be a
tracker name.

`get ticket <tracker> <id>`
: Fetch detailed information about a ticket. The arguments should be a tracker
name and ticket ID.

`create ticket <tracker>`
: Create a ticket in the given tracker. The argument should be a tracker name,
and both of the following options are required.

    `-f`, `--description <input>`
    : Specify an input file for the ticket description. If not given, or if
    `<input>` is "-", the ticket description is read from standard input.

    `--title <title>`
    : Specify the ticket title.

`update ticket <tracker> <id>`
: Update an existing ticket. The arguments should be a tracker name and ticket
ID, and at least one of the following options must be provided.

    `-f`, `--comment <input>`
    : Add a comment to the ticket. If not given, or if `<input>` is "-", the
    comment is read from standard input.

    `--status <status>`
    : Update the ticket status. The value must be a valid status type and
    should be provided as a lower-case string, for example "confirmed" or
    "resolved".

    `--resolution <resolution>`
    : Update the ticket resolution. The value must be a valid resolution type
    and should be provided as a lower-case string, for example "implemented" or
    "wont_fix".

`get events <tracker> <id>`
: Fetch a list of events that have occurred in a ticket. The arguments should
be a tracker name and ticket ID.

## OTHER

`help`
: Attempt to open this program's man page with `man(1)`. If that program could
not be executed, display this program's `--help` text.

`manual`
: Print this program's man page to standard output.

# PAGINATION

Commands that retrieve a list of items will always start from the most recent.
The total number of items can be controlled with the `--max-results` or `-n`
flag, which expects a numeric argument specifying the number of items to
retrieve. For example:

    # retrieve the five most recent jobs
    $ sourcehut get jobs -n 5

    # retrieve the single most recent paste
    $ sourcehut get pastes -n 1

# ENVIRONMENT VARIABLES

The following variables can be used as an alternative to command line flags.
They accept the same values and are validated in the same way. Any options
given on the command line take precedence.

`SOURCEHUT_CLI_OUTPUT`
: Corresponds to the `--output` option.

`SOURCEHUT_CLI_MAX_RESULTS`
: Corresponds to the `--max-results` option.

`SOURCEHUT_CLI_ACCESS_TOKEN`
: Corresponds to the `--access-token` option.
