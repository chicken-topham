let
  repo = "${builtins.toString ./.}/chicken";
  pkgs = import (import ./nix/sources.nix).nixpkgs {};
in pkgs.mkShell {
  CHICKEN_INSTALL_PREFIX = "${repo}";
  CHICKEN_INSTALL_REPOSITORY = "${repo}/lib";
  CHICKEN_REPOSITORY_PATH = "${repo}/lib";
  propagatedBuildInputs = with pkgs; [ chicken openssl pandoc ];
  shellHook = ''export PATH="${repo}/bin:$PATH"'';
}
