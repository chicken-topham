# chicken-sourcehut

Command line interface and [CHICKEN Scheme](https://call-cc.org/) client
library for [sourcehut](https://sourcehut.org/).

## Resources

 * Sources: <https://git.sr.ht/~evhan/chicken-sourcehut>
 * Issues: <https://todo.sr.ht/~evhan/chicken-sourcehut>
 * Manual: <https://foldling.org/sourcehut/manual.html>
 * Documentation: <https://api.call-cc.org/5/doc/sourcehut>

## Installation

### CHICKEN Scheme Extension Library

Use your distribution's package manager to install CHICKEN 5.2 or newer,
then run:

``` bash
chicken-install -s sourcehut
```

This will install the CHICKEN Scheme extension libraries, as well as the CLI
program, under `$CHICKEN_INSTALL_PREFIX/bin/sourcehut`.

On some platforms you may be required to install the OpenSSL development
libraries first.

### Command Line Interface

#### Prebuilt Binaries

If you just want to use the CLI program without installing CHICKEN first, you
can download prebuilt binaries for some common platforms [here][binaries].

[binaries]: https://foldling.org/sourcehut/

#### Nix

To build and install the package using [Nix][], you can run:

``` bash
nix-env -i -f https://git.sr.ht/~evhan/chicken-sourcehut/archive/master.tar.gz
```

[Nix]: https://nixos.org/

## Examples

### Using the CHICKEN Scheme Extension Library

Create a new job on [builds.sr.ht](https://builds.sr.ht/) and fetch
information about it:

``` scheme
(import (sourcehut)
        (sourcehut builds))

(access-token "your-access-token-goes-here")

(create (job manifest: "xyz"))
; => ((#:service "builds" #:path "/api/jobs")
;     (id . 1234))

(retrieve (job 1234))
; => ((#:service "builds" #:path "/api/jobs/1234")
;     (id . 1234)
;     (status . "running")
;     (setup_log ...)
;     (tasks . #(...))
;     (runner ...))

(retrieve (manifest 1234))
; => "xyz"
```

### Using the Command Line Interface

As above, but using the `sourcehut` CLI program.

``` bash
$ export SOURCEHUT_CLI_ACCESS_TOKEN="your-access-token-goes-here"

$ sourcehut create job --manifest build.yaml --note "Example build"
((id . 1234))

$ sourcehut get job 1234
((id . 1234)
 (status . "running")
 (setup_log ...)
 (tasks . #(...))
 (runner ...))

$ sourcehut get job 1234 -o json | jq
{
  "id": 1234,
  "status": "running",
  "setup_log": ...,
  "tasks": [...],
  "note": "Example build",
  "runner": ...
}
```

### Using the CLI in a Build Manifest

To use the CLI within a Linux-based build manifest, you'll need to [create
a secret file](https://builds.sr.ht/secrets) containing an access token, add it
to your job configuration, and set `SOURCEHUT_CLI_ACCESS_TOKEN` to that file
path. Then, you can download and run the `sourcehut` binary in your build tasks
without exposing the token. For example:

``` yaml
image: alpine/latest
packages:
  - wget
  - jq
secrets:
  # add a secret containing a personal access token in /home/build/.token
  - 43b6093b-85f0-4a19-97c9-9758a447e5fa
environment:
  # specify the path to the access token
  SOURCEHUT_CLI_ACCESS_TOKEN: /home/build/.token
  SOURCEHUT_CLI_OUTPUT: json
tasks:
  # install the program
  - setup: |
      sudo wget -qO /usr/bin/sourcehut https://foldling.org/sourcehut/sourcehut-x86_64-linux
      sudo chmod +x /usr/bin/sourcehut
  # use the program
  - usage: |
      sourcehut get jobs -n1 | jq
```

## Documentation

Full API documentation for the Scheme library is available on
[wiki.call-cc.org][] and [api.call-cc.org][].

For CLI documentation, refer to the program's man page. This is available
online [here][man-html], in Markdown format [here][man-markdown], or via the
`sourcehut help` command once the program is installed. You can also view the
latest version in your terminal without installing the program first by
running:

``` bash
wget -qO- https://git.sr.ht/~evhan/chicken-sourcehut/blob/master/sourcehut.1 | man /dev/stdin
```

[wiki.call-cc.org]: https://eggs.call-cc.org/5/sourcehut
[api.call-cc.org]: https://api.call-cc.org/5/doc/sourcehut
[man-html]: https://foldling.org/sourcehut/manual.html
[man-markdown]: https://git.sr.ht/~evhan/chicken-sourcehut/blob/master/sourcehut.1.md

## Development

To build the software locally:

``` bash
chicken-install -no-install
```

To build the man page as `roff(7)` or HTML:

``` bash
pandoc -s -i sourcehut.1.md -o sourcehut.1
pandoc -s -i sourcehut.1.md -o sourcehut.html --toc --section-divs --css https://foldling.org/html/files/css/main.css
```

Nix users can use `nix-shell` to pull in dependencies for local development, or
run `nix-build` to produce a packaged version of the CLI application. The output
will be linked from `./result` and can be called via `./result/bin/sourcehut`.

## License

Three-clause BSD. See LICENSE for details.
