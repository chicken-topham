;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Tests for the sourcehut extension.
;;;
;;; Copyright (c) 2019-2020 Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(import (chicken pathname)
        (chicken platform)
        (chicken process-context)
        (chicken string)
        (test))

(set! (current-directory)
  (normalize-pathname (make-pathname (program-name) "../..")))

(let ((sourcehut "./cli --help >/dev/null")
      (sourcehut-usage "Usage: ./cli command [options ...]"))
  (import (chicken io)
          (chicken process))
  (test 0 (system sourcehut))
  (let ((output (with-input-from-pipe "./cli --help" read-lines)))
    (test-assert (member sourcehut-usage output))))

(define ((service name) path . rest)
  `((#:service ,name #:path ,(conc "/api" path)) . ,rest))

(repository-path (cons (current-directory) (repository-path)))

(import (sourcehut))

(let ((builds (service "builds")))
  (import (sourcehut builds))
  (test (builds "/jobs/1")
        (job 1))
  (test (builds "/jobs" '(manifest . "hello"))
        (job manifest: "hello"))
  (test (builds "/jobs/1/start")
        (start 1))
  (test (builds "/jobs/1/cancel")
        (cancel 1)))

(let ((todo (service "todo")))
  (import (sourcehut todo))
  (test (todo "/trackers/project/tickets/1")
        (ticket "project" 1))
  (test (todo "/user/~user/trackers/project/tickets/1")
        (ticket "~user" "project" 1))
  (test (todo "/trackers/project/tickets/1" '(status . "resolved"))
        (ticket "project" 1 status: "resolved"))
  (test (todo "/trackers/project/tickets" '(title . "title"))
        (ticket "project" title: "title"))
  (test (todo "/trackers/project/tickets" '(title . "title") '(description . "description"))
        (ticket "project" title: "title" description: "description")))

(unless (get-environment-variable "SRHT_ACCESS_TOKEN")
  (print "skipping remaining tests (no access token)")
  (exit))

(let ()
  (import (sourcehut meta))
  (test-assert (retrieve (profile)))
  (test-assert (retrieve (audit-log)))
  (test-assert (retrieve (ssh-keys)))
  (test-assert (retrieve (pgp-keys))))

(let ()
  (import (sourcehut paste))
  (test-assert (retrieve (pastes))))

(let ()
  (import (sourcehut git))
  (test-assert (retrieve (refs "chicken-sourcehut")))
  (test-assert (retrieve (refs "~evhan" "chicken-sourcehut")))
  (test-assert (retrieve (log "chicken-sourcehut")))
  (test-assert (retrieve (log "~evhan" "chicken-sourcehut"))))

(let ()
  (import (sourcehut todo))
  (test-assert (retrieve (trackers)))
  (test-assert (retrieve (trackers "~evhan")))
  (test-assert (retrieve (tracker "chicken-sourcehut")))
  (test-assert (retrieve (tracker "~evhan" "chicken-sourcehut")))
  (test-assert (retrieve (tickets "chicken-sourcehut")))
  (test-assert (retrieve (tickets "~evhan" "chicken-sourcehut")))
  (test-assert (retrieve (ticket "chicken-sourcehut" 1)))
  (test-assert (retrieve (ticket "~evhan" "chicken-sourcehut" 1)))
  (test-assert (retrieve (events "chicken-sourcehut" 1)))
  (test-assert (retrieve (events "~evhan" "chicken-sourcehut" 1))))

(define (cli . args)
  (import (chicken io)
          (chicken process)
          (chicken string))
  (with-input-from-pipe (string-intersperse (cons "./cli" args)) read))

(let ()
  (test-assert (list?   (cli "get" "profile")))
  (test-assert (vector? (cli "get" "audit-log")))
  (test-assert (vector? (cli "get" "ssh-keys")))
  (test-assert (vector? (cli "get" "pgp-keys")))
  (test-assert (vector? (cli "get" "jobs")))
  (test-assert (vector? (cli "get" "pastes")))
  (test-assert (vector? (cli "get" "trackers")))
  (test-assert (vector? (cli "get" "refs" "chicken-sourcehut")))
  (test-assert (vector? (cli "get" "tickets" "chicken-sourcehut")))
  (test-assert (list?   (cli "get" "ticket" "chicken-sourcehut" "1")))
  (test-assert (vector? (cli "get" "events" "chicken-sourcehut" "1"))))

(test-exit)
