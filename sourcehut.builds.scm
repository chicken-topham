;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; REST API bindings for paste.sr.ht
;;;
;;; Copyright (c) 2019-2020, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare (module (sourcehut builds))
         (export job jobs manifest start cancel))

(import (chicken format)
        (chicken keyword)
        (chicken type)
        (sourcehut))

(define-inline (make-crud path #!optional (body '()))
  `((#:service "builds" #:path ,path) . ,body))

(define-inline (make-job #!rest args)
  (keyword-arguments->alist args))


;;
;; https://man.sr.ht/builds.sr.ht/api.md#get-apijobs
;;
(: jobs (-> (list-of pair)))
(define (jobs)
  (make-crud (format "/api/jobs")))

;;
;; https://man.sr.ht/builds.sr.ht/api.md#get-apijobsidmanifest
;;
(: manifest (integer -> (list-of pair)))
(define (manifest id)
  (make-crud (format "/api/jobs/~A/manifest" id)))

;;
;; https://man.sr.ht/builds.sr.ht/api.md#post-apijobsidstart
;;
(: start (integer -> (list-of pair)))
(define (start id)
  (make-crud (format "/api/jobs/~A/start" id)))

;;
;; https://man.sr.ht/builds.sr.ht/api.md#post-apijobsidcancel
;;
(: cancel (integer -> (list-of pair)))
(define (cancel id)
  (make-crud (format "/api/jobs/~A/cancel" id)))

;;
;; https://man.sr.ht/builds.sr.ht/api.md#get-apijobsid
;; https://man.sr.ht/builds.sr.ht/api.md#post-apijobs
;;
(: job (#!rest any -> (list-of pair)))
(define (job #!optional id #!rest details)
  (cond
    ((integer? id)
     (make-crud (format "/api/jobs/~A" id)))
    ((get-keyword #:manifest (cons id details))
     (make-crud "/api/jobs" (apply make-job id details)))
    (else
     (signal-condition
      '(sourcehut)
      '(arity)
      '(exn location job message "job id or #:manifest must be given")))))
