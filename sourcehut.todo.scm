;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; REST API bindings for todo.sr.ht
;;;
;;; Copyright (c) 2020, Evan Hanson
;;;
;;; See LICENSE for details.
;;;

(declare (module (sourcehut todo))
         (export events ticket tickets tracker trackers))

(import (chicken format)
        (chicken keyword)
        (chicken type)
        (sourcehut))

(define-inline (make-crud path #!optional (body '()))
  `((#:service "todo" #:path ,path) . ,body))

(define-inline (make-tracker #!key (name 'null) (description 'null))
  `((name . ,name)
    (description . ,description)))

;;
;; https://man.sr.ht/todo.sr.ht/api.md#get-apitrackers
;; https://man.sr.ht/todo.sr.ht/api.md#get-apiuserusernametrackers
;;
(: trackers (#!optional string -> (list-of pair)))
(define (trackers #!optional username)
  (make-crud
   (if (not username)
       (format "/api/trackers")
       (format "/api/user/~A/trackers" username))))

;;
;; https://man.sr.ht/todo.sr.ht/api.md#get-apitrackerstracker-name
;; https://man.sr.ht/todo.sr.ht/api.md#get-apiuserusernametrackerstracker-name
;; https://man.sr.ht/todo.sr.ht/api.md#put-apitrackerstracker-name
;; https://man.sr.ht/todo.sr.ht/api.md#post-apitrackers
;;
(: tracker (#!rest any -> (list-of pair)))
(define (tracker #!optional username-or-tracker-name tracker-name #!rest details)
  (cond
    ;; update
    ((and (string? username-or-tracker-name)
          (get-keyword #:description (cons tracker-name details)))
     (make-crud (format "/api/trackers/~A" username-or-tracker-name)
                (apply make-tracker
                       name: username-or-tracker-name
                       tracker-name details)))
    ;; fetch
    ((string? username-or-tracker-name)
     (make-crud
      (if (not tracker-name)
          (format "/api/trackers/~A" username-or-tracker-name)
          (format "/api/user/~A/trackers/~A" username-or-tracker-name tracker-name))))
    ;; create
    ((get-keyword #:name (append (list username-or-tracker-name tracker-name) details))
     (make-crud
      "/api/trackers"
      (apply make-tracker username-or-tracker-name tracker-name details)))
    (else
     (signal-condition
      '(sourcehut)
      '(arity)
      '(exn location tracker message "tracker name or #:name must be given")))))

;;
;; https://man.sr.ht/todo.sr.ht/api.md#get-apitrackerstracker-nametickets
;; https://man.sr.ht/todo.sr.ht/api.md#get-apiuserusernametrackerstracker-nametickets
;;
(: tickets (string #!optional string -> (list-of pair)))
(define (tickets username-or-tracker-name #!optional tracker-name)
  (make-crud
   (if (not tracker-name)
       (format "/api/trackers/~A/tickets" username-or-tracker-name)
       (format "/api/user/~A/trackers/~A/tickets" username-or-tracker-name tracker-name))))

;;
;; https://man.sr.ht/todo.sr.ht/api.md#get-apitrackerstracker-nameticketsticket-id
;; https://man.sr.ht/todo.sr.ht/api.md#get-apiuserusernametrackerstracker-nameticketsticket-id
;; https://man.sr.ht/todo.sr.ht/api.md#put-apitrackerstracker-nameticketsticket-id
;; https://man.sr.ht/todo.sr.ht/api.md#post-apitrackerstracker-nametickets
;;
(: ticket (string #!rest any -> (list-of pair)))
(define (ticket username-or-tracker-name #!optional tracker-name-or-ticket-id ticket-id #!rest details)
  (cond
    ;; create
    ((and (string? username-or-tracker-name)
          (keyword? tracker-name-or-ticket-id))
     (make-crud
      (format "/api/trackers/~A/tickets" username-or-tracker-name)
      (keyword-arguments->alist (append (list tracker-name-or-ticket-id ticket-id) details))))
    ;; update
    ((and (string? username-or-tracker-name)
          (or (string? tracker-name-or-ticket-id)
              (number? tracker-name-or-ticket-id))
          (keyword? ticket-id))
     (make-crud
      (format "/api/trackers/~A/tickets/~A" username-or-tracker-name tracker-name-or-ticket-id)
      (keyword-arguments->alist (cons ticket-id details))))
    ;; fetch
    ((and (string? username-or-tracker-name)
          (or (string? tracker-name-or-ticket-id)
              (number? tracker-name-or-ticket-id)))
     (make-crud
      (if (not ticket-id)
          (format "/api/trackers/~A/tickets/~A" username-or-tracker-name tracker-name-or-ticket-id)
          (format "/api/user/~A/trackers/~A/tickets/~A" username-or-tracker-name tracker-name-or-ticket-id ticket-id))))
    (else
     (signal-condition
      '(sourcehut)
      '(arity)
      '(exn location ticket message "ticket id must be given")))))

;;
;; https://man.sr.ht/todo.sr.ht/api.md#get-apitrackerstracker-nameticketsticket-idevents
;; https://man.sr.ht/todo.sr.ht/api.md#get-apiuserusernametrackerstracker-nameticketsticket-idevents
;;
(: events (string string #!optional string -> (list-of pair)))
(define (events username-or-tracker-name tracker-name-or-ticket-id #!optional ticket-id)
  (make-crud
   (if (not ticket-id)
       (format "/api/trackers/~A/tickets/~A/events" username-or-tracker-name tracker-name-or-ticket-id)
       (format "/api/user/~A/trackers/~A/tickets/~A/events" username-or-tracker-name tracker-name-or-ticket-id ticket-id))))
