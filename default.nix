let
  eggsHash = "sha256:088jzdkv3zp2b23a98wcjjcycg3ai59x912byma7dil3wkvc50fb";
in {
  pkgs ? import (import ./nix/sources.nix).nixpkgs {},
  beaker ? import (fetchTarball https://git.sr.ht/~evhan/beaker/archive/0.0.13.tar.gz) { inherit pkgs; }
}:
with pkgs; with beaker; eggProgram {
  name = "chicken-sourcehut";
  src = ./.;
  buildInputs = [ openssl pkg-config ];
  eggCache = eggCache {
    hash = eggsHash;
    eggs = ./sourcehut.egg.lock;
  };
  postInstall = ''
    wrapProgram $out/bin/sourcehut --prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath [ openssl ]}
  '';
}
